﻿using UnityEngine;

namespace Prototype
{
    public class Client : MonoBehaviour
    {
        public Drone m_Drone;
        public Sniper m_Sniper;
        public EnemySpawner m_Spawner;

        private Enemy m_Spawn;
        private int m_IncrementorDrone = 0;
        private int m_incrementorSniper = 0;

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.D))
            {
                m_Spawn = m_Spawner.SpawnEnemy(m_Drone);

                m_Spawn.name = "Drone_Clone_" + ++m_IncrementorDrone;
                m_Spawn.transform.Translate(Vector3.forward * m_IncrementorDrone * 1.5f);
            }

            if(Input.GetKeyDown(KeyCode.S))
            {
                m_Spawn = m_Spawner.SpawnEnemy(m_Sniper);

                m_Spawn.name = "Sniper_Clone_" + ++m_incrementorSniper;
                m_Spawn.transform.Translate(Vector3.forward * m_incrementorSniper * 1.5f);
            }
        }
    }
}
