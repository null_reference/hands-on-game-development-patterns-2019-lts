﻿using UnityEngine;

namespace Prototype
{
    public class EnemySpawner : MonoBehaviour
    {
        public ICopyable m_Copy;

        public Enemy SpawnEnemy(Enemy prototype)
        {
            m_Copy = prototype.Copy();
            return (Enemy)m_Copy;
        }
    }
}
