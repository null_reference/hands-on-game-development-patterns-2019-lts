﻿namespace Prototype
{
    public interface ICopyable
    {
        ICopyable Copy();
    }
}